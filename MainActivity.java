package com.hosea.ochieng.navkenya;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class MainActivity extends AppCompatActivity{
    private Button button;
    WifiManager wifiManager;
    BroadcastReceiver broadcastReceiver;
    List<ScanResult> wifiList;
    TextView textView;
    TextView wifiListInfoDisp;
    String display = "";
    StringBuilder sb = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.getInfBtn);
        textView = (TextView)findViewById(R.id.wifinfoDisp);
        wifiListInfoDisp = (TextView)findViewById(R.id.wifiListInfoDisp);

        //info about connected wifi
        wifiManager=(WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo= wifiManager.getConnectionInfo();
        textView.append("\n\nWifi Info:" + wifiInfo.toString());


        //show the number of access point when this button is clicked
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wifiListInfoDisp.setText("Number of wifis available:");
                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        List<ScanResult> results = wifiManager.getScanResults();

                        //update the number of available access points
                        wifiListInfoDisp.setText(results.size());
                    }
                };
                registerReceiver(broadcastReceiver, new IntentFilter(
                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION
                ));
                wifiManager.startScan();
            }
        });

        //list access points

        /*wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiReceiver = new WifiReceiver();

        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
        textView.setText(sb);

        /*final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                if (intent.getAction() == WifiManager.SCAN_RESULTS_AVAILABLE_ACTION) {
                    List<ScanResult> mScanResults = wifiManager.getScanResults();
                    textView.setText(mScanResults.size());
                }
            }
        };*/



        /*button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0){
                getConnectionRSSI();
                //wifiScansDetails();
                //textView.setText(display);
            }
        });*/
    }

    /*@Override
    public void onClick(View v) {
        wifiListInfoDisp.setText("Number of wifis available:");
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    List<ScanResult> results = wifiManager.getScanResults();
                    wifiListInfoDisp.setText(results.size());
                }
            };
    }

    public void getConnectionRSSI(){
        connection = wifiManager.getConnectionInfo();
        display += "SSID: "+connection.getBSSID() +"\n"+"RSSI: "+connection.getRssi()+"\n"+"Mac Address: "+connection.getMacAddress();
        textView.setText(display);
    }*/

    class WifiScanner extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            wifiList = wifiManager.getScanResults();

            for(int i = 0; i < wifiList.size(); i++) {
                ScanResult scan = wifiList.get(i);

                    sb.append(scan.SSID+"\t"+scan.BSSID+"\t"+scan.level+"\t"+"\n");
                }
            }
            //textView.setText(sb);
        }
}
